package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    protected @NotNull Class<Project> getEntityClass() {
        return Project.class;
    }

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}